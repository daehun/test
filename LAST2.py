import re
import urllib.request

from bs4 import BeautifulSoup

from flask import Flask
from slack import WebClient
from slackeventsapi import SlackEventAdapter
from slack.web.classes import extract_json
from slack.web.classes.blocks import *


SLACK_TOKEN = 'xoxb-686561524084-689740504837-FmggGTih285yu5yhmJv7ambQ'
SLACK_SIGNING_SECRET = '2c7c95c694e2bfd1d1a7bcf92ed6487f'


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 받습니다.
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)


# 크롤링 함수 구현하기
def GetText(op, gameType):
    topX = 15
    url = "https://store.steampowered.com/tags/en/" + gameType + "/"

    sourcecode = urllib.request.urlopen(url).read()     
    soup = BeautifulSoup(sourcecode, "html.parser")  
    
    if op in "new and treding":
        cat = soup.find("div", id="tab_content_NewReleases")
    elif op in "top selling":
        cat = soup.find("div", id="tab_content_TopSellers")
    elif op in "popular":
        cat = soup.find("div", id="tab_content_ConcurrentUsers")
    elif op in "upcoming":
        cat = soup.find("div", id="tab_content_ComingSoon")

    results = []
    count = 1

    contents = cat.find_all("a")
    for content in contents:
        if len(results) >= topX:
            break

        title = content.find("div", class_="tab_item_name") # 제목 파싱
        discountPct = content.find("div", class_="discount_pct") # 할인율 파싱
        originPrice = content.find("div", class_="discount_original_price") # 원가 파싱
        finalPrice = content.find("div", class_="discount_final_price") # 최종 가격 파싱
        
        line = str(count) + ". " + title.get_text().strip() + ("\t" * 2)

        if originPrice != None: 
            originPrice = originPrice.get_text().strip()
            finalPrice = finalPrice.get_text().strip()
            discountPct = discountPct.get_text().strip()
            line += ("[~"+originPrice +"~]" + " ----> " + "[" + finalPrice + "]  "+ "("+discountPct + ")")
        else :
            if finalPrice != None:
                finalPrice = finalPrice.get_text().strip()
                line += ("[" + finalPrice + "]")
            else:
                line += ("[ Unknown ]")
        
        results.append((line))
        count += 1

    return results

def GetImgURL(op, gameType):
    topX = 15
    url = "https://store.steampowered.com/tags/en/" + gameType + "/"

    sourcecode = urllib.request.urlopen(url).read()     
    soup = BeautifulSoup(sourcecode, "html.parser")  
    
    if op in "new and treding":
        cat = soup.find("div", id="tab_content_NewReleases")
    elif op in "top selling":
        cat = soup.find("div", id="tab_content_TopSellers")
    elif op in "popular":
        cat = soup.find("div", id="tab_content_ConcurrentUsers")
    elif op in "upcoming":
        cat = soup.find("div", id="tab_content_ComingSoon")

    results = []
    contents = cat.find_all("a")
    for content in contents:
        if len(results) >= topX:
            break

        # 이미지 URL 파싱
        imgTag = str(content.find("img")) # 태그 부분 추출
        temp = imgTag.split() # src 추출
        url = str(temp[2][5:len(temp[2]) - 3]) # src 값 추출

        results.append((url))
 
    return results

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]

    temp = text.split()
    
    # 명령어 종류 선정
    op = temp[1].lower() # top, new...
    isValidOp = False

    if op in "new and treding":
        isValidOp = True
    elif op in "top selling":
       isValidOp = True
    elif op in "popular":
        isValidOp = True
    elif op in "upcoming":
       isValidOp = True

    # 명령어가 유효하지 않을 경우 사용법을 출력한다.
    if isValidOp == False :
        message = "선호타입(new, top, popular, upcoming) 게임종류(action,rpg,multi,sports..등등) 순으로 입력해주세요!"
        slack_web_client.chat_postMessage(channel = channel, text = message)

    else :
        # 게임 종류 선정
        isValidGameType = False
        wantFor = temp[2].lower()
        gameType = str()
        if wantFor in "action" :
            gameType = "Action"
            isValidGameType = True
        elif wantFor in "rpg" :
            gameType = "RPG"
            isValidGameType = True
        elif wantFor in "racing" :
            gameType = "Racing"
            isValidGameType = True
        elif wantFor in "massively multiplayer" :
            gameType = "Massively%20Multiplayer"
            isValidGameType = True
        elif wantFor in "adventure" :
            gameType = "Adventure"
            isValidGameType = True
        elif wantFor in "casual" :
            gameType = "Casual"
            isValidGameType = True
        elif wantFor in "indie" :
            gameType = "Indie"
            isValidGameType = True
        elif wantFor in "simulation" :
            gameType = "Simulation"
            isValidGameType = True
        elif wantFor in "sports" :
            gameType = "Sports"
            isValidGameType = True
        elif wantFor in "strategy" :
            gameType = "Strategy"
            isValidGameType = True
            
        if isValidGameType == False:
            message = "선호타입(new, top, popular, upcoming) 게임종류(action,rpg,multi,sports..등등) 순으로 입력해주세요!"
            slack_web_client.chat_postMessage(channel = channel, text = message)

        else :
            message = GetText(op, gameType)
            imgurl = GetImgURL(op, gameType)
            
            for i in range(0, 15):
                slack_web_client.chat_postMessage(channel = channel, text = message[i])
                img = ImageBlock(image_url = imgurl[i], alt_text = " ")
                blocks = [img]
                slack_web_client.chat_postMessage(channel = channel, blocks = extract_json(blocks))
            
# / 로 접속하면 서버가 준비되었다고 알려줍니다.
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=5000)

